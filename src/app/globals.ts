import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable() 
export class Globals {
  public selectedFunction:BehaviorSubject<string> = new BehaviorSubject<string>('Avatar');
  public showOptions:BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  public currentOptions:BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  setFunction(func){
    this.selectedFunction.next(func);
  }

  setShowOptions(bool){
    this.showOptions.next(bool);
  }

  setOptions(options){
    this.currentOptions.next(options);
  }
}