import { 
    Component,
    OnInit,
    ElementRef
} from '@angular/core';

import { Observable } from 'rxjs';

import { Message } from '../message/message.model';
import { MessageService } from '../message/message.service';
import { Globals } from '../globals';

@Component({
    selector: 'chat-box',
    templateUrl: './chat-box.component.html',
    styleUrls: [ './chat-box.component.css' ]
})

export class ChatBoxComponent implements OnInit {
    messages: Message[];
    inputMessage: Message;
    showOptions: Boolean;
    options: string[];
    dateNow: Date;

    constructor(
        private messageService: MessageService,
        private el: ElementRef,
        private globals: Globals
      ) {
          this.globals.showOptions.subscribe(value => setTimeout(() => this.showOptions = value, 0));
          this.globals.currentOptions.subscribe(value => setTimeout(() => this.options = value, 0));
      }  

    //getMessages(): void{
    //    this.messageService.getMessages()
    //    .subscribe(messages => this.messages = messages);
    //}
    
    ngOnInit(): void {
        this.dateNow = new Date();
        this.messages = new Array<Message>();
        //this.getMessages();
        this.inputMessage = new Message();
    }

    openImg(event: any): void {
        var input = event.target;
        var fileReader: FileReader = new FileReader();

        fileReader.onloadend = function(e) {
            var imgUrl = fileReader.result;
        }

        fileReader.readAsDataURL(input.files[0]);
    }

    onEnter(event: any): void {
        if(this.inputMessage.text != null){
            if(this.inputMessage.text.trim().length > 0){
                var message = Object.assign({}, this.inputMessage);
                this.sendMessage(message);
            }
            this.inputMessage = new Message();
        }
        event.preventDefault();
    }

    sendMessage(message: Message): void{
        this.removeTempSpace();
        message.sentAt = new Date();
        message.author = 'user';
        //this.messageService.addMessage(message).subscribe(message => {this.responseMessage.text = message['base'];});
        var responseMessage = this.messageService.postToEngine(message);
        this.pushMessage(message).then(() => {
            setTimeout(() => {
              this.scrollToBottom();
            });
        });
        
        if(responseMessage != null){
            setTimeout(() => {this.botResponse(responseMessage);}, 500);  
        }
        //if(!message.isOptionReply){
         //   var echoMessage = Object.assign({}, message);
            
        //    if(message.text.trim()=='options'){
         //       setTimeout(() => {this.botOptions(echoMessage);}, 500);
         //   }else{
         //       setTimeout(() => {this.botEcho(echoMessage);}, 500);
         //   }
        //}  
    }

    botResponse(message: Message): void{
        this.pushMessage(message).then(() => {
            setTimeout(() => {
              this.scrollToBottom();
            });
        });
    }

    /*botEcho(message: Message): void{
        message.sentAt = new Date(); 
        message.author = 'bot';
        this.messageService.addMessage(message);
        this.pushMessage(message).then(() => {
            setTimeout(() => {
              this.scrollToBottom();
            });
        });
    }

    botOptions(message: Message): void{
        message.text = 'Please select an option';
        message.options = ['Login','Avatar'];
        message.sentAt = new Date(); 
        message.author = 'bot';
        this.messageService.addMessage(message);
        this.pushMessage(message).then(() => {
            setTimeout(() => {
              this.insertTempSpace();
            });
        }).then(() => {
            setTimeout(() => {
              this.scrollToBottom();
            });
        });
    }*/

    pushMessage(message: Message): Promise<number>{
        return Promise.resolve(this.messages.push(message));
    }

    onSelect(option): void{
        this.globals.setFunction(option);
        this.globals.setShowOptions(false);
        this.removeTempSpace();
        var replyMessage = new Message();
        replyMessage.text = option;
        replyMessage.isOptionReply = true;
        this.sendMessage(replyMessage);
    }

    scrollToBottom(): void{
        var div = document.getElementById('scrollbar');
        div.scrollTop = div.scrollHeight;
    }

    insertTempSpace(): void{
        var div = document.getElementById('scrollbar');
        div.insertAdjacentHTML('beforeend', '<div id="temp-space" style="height:45px; width:100%; clear:both;"></div>')
    }

    removeTempSpace(): void{
        var div = document.getElementById('temp-space');
        if(div!=null){
            div.remove();
        }
    }
}