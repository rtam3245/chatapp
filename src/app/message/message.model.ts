export class Message {
    id: number;
    sentAt: Date;
    author: string;
    text: string;
    options: string[];
    isOptionReply: Boolean;

    constructor(obj?: any) {
        this.sentAt = obj && obj.sentAt || null;
        this.author = obj && obj.author   || null;
        this.text   = obj && obj.text   || null;
        this.options   = obj && obj.options  || [];
        this.isOptionReply   = obj && obj.isOptionReply  || false;
    }
}