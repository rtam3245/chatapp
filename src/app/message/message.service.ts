import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/toPromise';

import { Message } from './message.model';

@Injectable()
export class MessageService {
    private messageUrl = 'http://localhost:5000/bot/api/v1.0';
    private headers = new Headers({'Content-Type': 'application/json'});
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
      }
    private handleObservableError(error: any): Observable<any> {
    console.error('An error occurred', error);
    return Observable.throw(error.json().error || 'Server error');
    }
    constructor(private http: HttpClient) { }

    postToEngine(postMessage: Message): Message{
        var responseMessage = new Message;
        responseMessage.author = 'bot';
        responseMessage.sentAt = new Date();
        
        this.http.post(this.messageUrl, postMessage)
        .subscribe(data => {
            responseMessage.text = data['out'];
        }, err => {
            console.log('Something went wrong!');
        });

        return responseMessage;
    }

    /*getMessages(): Observable<Message[]> {
      return this.http
        .get(this.messagesUrl)
        .map((res:Response) => res.json().data as Message[])
        .catch(this.handleObservableError);
    }

    addMessage(message: Message): Observable<any> {
        return this.http
            .get(this.messagesUrl)//, JSON.stringify({text: message.text, author: message.author, sentAt: message.sentAt}), {headers: this.headers})
            .map((res:Response) => res.json().data)
            .catch(this.handleObservableError);
    }*/
}