import { 
    Component,
    OnInit,
    Input } from '@angular/core';

import { Observable } from 'rxjs';

import { Message } from './../message/message.model';
import { Globals } from '../globals';

@Component({
    selector: 'chat-message',
    templateUrl: './chat-message.component.html',
    styleUrls: [ './chat-message.component.css' ]
})

export class ChatMessageComponent implements OnInit {
    @Input() message: Message;
    options: string[];
    isBot: boolean;

    constructor(private globals: Globals){}

    ngOnInit(): void{
        this.options = this.message.options;

        if(this.message.author=='bot'){
            this.isBot = true;
        }else if(this.message.author=='user'){
            this.isBot = false;
        }

        if(this.message.options.length > 0){
            this.globals.setShowOptions(true);
            this.globals.setOptions(this.message.options);
        }else{
            this.globals.setShowOptions(false);
            this.globals.setOptions([]);
        }
    }
}