import { Component } from '@angular/core';
import { Globals } from '../globals';

@Component({
    selector: 'chat-function-container',
    templateUrl: './chat-function-container.component.html',
    styleUrls: [ './chat-function-container.component.css' ]
})

export class ChatFunctionContainerComponent {
    selected: string;
    username = "";
    password = "";

    constructor(private globals: Globals){
        this.globals.selectedFunction.subscribe(value => this.selected = value);
    }
}
